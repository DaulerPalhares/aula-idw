import Vue from 'vue/dist/vue.common.js';
import sayHello from './sayHello';

Vue.component('button-counter', {
    data: function () {
      return {
        count: 0
      }
    },
    template: '<button v-on:click="count++">Você clicou em mim {{ count }} vezes.</button>'
});
Vue.component('alert-box', {
    template: `
      <div class="demo-alert-box">
        <strong>Error!</strong>
        <slot></slot>
      </div>
    `
});

const app = new Vue({
    el:'#app',
    data: {
        message: sayHello('IDW'),
        seen: true
    },
    methods: {
        showMessage(){
          this.seen = true
        },
        hideMessage(){
          this.seen = false
        },
        messageHided :() =>{
            return "---- ---"
        }
    }
});

