
import mysql from 'mysql';
import dotenv from 'dotenv';

var con;

module.exports.actualConnection = function createConnection(){ 
    dotenv.config();
    con = mysql.createConnection({
        host: process.env.DB_SERVER_URL,
        port: process.env.DB_SERVER_PORT,
        user: process.env.DB_USER_NAME,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
        insecureAuth : true
    });
    console.log("URL" +process.env.DB_SERVER_URL + "PORT" + process.env.DB_SERVER_PORT)    
    return con;
};
module.exports.checkConnection = function(done){con.connect(done)};

module.exports.createDatabase = function createDatabase(name){
    con.query(`CREATE DATABASE ${name}`,function (err,result){
        if(err){
            if(err.code === "ER_DB_CREATE_EXISTS"){
                return;
            }
            else{
                throw err;
            }
        }
    });
};
