import { equal } from 'assert'
import sayHello from '../src/sayHello'

describe('sayHello test',() => {
  it('should return greet with with excitement',() => {
    equal(sayHello('test'), 'Hello test!');
  });
});