import { equal } from 'assert'
import connection from '../src/MysqlConnection';

describe('Access to DB', function(){
    it('Should connect to the test database', (done) =>{
        connection.actualConnection();    
        connection.checkConnection(done);
        connection.createDatabase('test');
    });
 });