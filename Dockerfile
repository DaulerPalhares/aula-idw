FROM node:11-alpine
# Copy current directory but keep away 
COPY . .
RUN npm ci
RUN npx gulp browserify
EXPOSE 3000
CMD [ "npm", "start" ]